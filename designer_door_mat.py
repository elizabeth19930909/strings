n, m = map(int, raw_input().split())

for i in range(n//2):
    print ('.|.'*(2*i + 1)).center(m, '-')

print 'WELCOME'.center(m, '-')

for i in range(n//2):
    print ('.|.'*(n - (i+1)*2)).center(m, '-')