def swap_case(s):
    return ''.join([el.upper() if el.islower() else el.lower() for el in s])

if __name__ == '__main__':
    s = raw_input()
    result = swap_case(s)
    print result