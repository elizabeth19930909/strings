def solve(s):
    for word in s.split():
        s = s.replace(word, word.capitalize())
    return s

s = raw_input()
result = solve(s)
print result