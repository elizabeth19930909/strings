def print_rangoli(n):
    myStr = 'abcdefghijklmnopqrstuvwxyz'[0:n]

    for i in range(1-n, n, 1):
        i = abs(i)
        line = myStr[n:i:-1] + myStr[i] + myStr[i+1:n]
        print ("--" * i + '-'.join(line) + "--" * i)

if __name__ == '__main__':
    n = int(raw_input())
    print_rangoli(n)