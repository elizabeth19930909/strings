def count_substring(string, sub_string):
    count = sum([1 for i in range(0, len(string)) if string[i:i + len(sub_string)] == sub_string])
    return count


if __name__ == '__main__':
    string = raw_input().strip()
    sub_string = raw_input().strip()

    count = count_substring(string, sub_string)
    print count